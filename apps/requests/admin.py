from django.contrib import admin
from django.contrib.admin import ModelAdmin

from apps.requests.models import RequestModel


@admin.register(RequestModel)
class RequestModelAdmin(ModelAdmin):
    list_display = ('method', 'url', 'timestamp', 'user')
    sortable_by = ('timestamp', 'user', 'method')
    ordering = ('-timestamp',)
    readonly_fields = (
        'method', 'url', 'timestamp', 'user', 'encoding',
        'content_type'
    )
