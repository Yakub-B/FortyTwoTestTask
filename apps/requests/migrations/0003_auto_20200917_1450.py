# Generated by Django 3.0.8 on 2020-09-17 14:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('requests', '0002_auto_20200917_1423'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requestmodel',
            name='encoding',
            field=models.CharField(max_length=10, null=True),
        ),
    ]
