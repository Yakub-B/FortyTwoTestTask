# Generated by Django 3.0.8 on 2020-09-19 10:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('requests', '0006_auto_20200918_0825'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='requestmodel',
            name='host',
        ),
        migrations.RemoveField(
            model_name='requestmodel',
            name='path',
        ),
        migrations.RemoveField(
            model_name='requestmodel',
            name='scheme',
        ),
        migrations.AddField(
            model_name='requestmodel',
            name='url',
            field=models.URLField(default='http://127.0.0.1:8000/', max_length=300),
            preserve_default=False,
        ),
    ]
